const fs = require('fs-extra');

async function init(path){
  const langs = (await fs.readdir('content')).filter(str=>str!=='common');
  for(let lang of langs){
    await fs.copy(`${path}/style.css`,`${path}/${lang}/style.css`,{dereference:true});
  }
}
init('generated/toPublish');
