# Tarifs
<br>
<div style="border:3px solid #10cbdf;background-color:#e7f9fb;color:#000000;text-align:center; font-weight:bold;">Je vous informe grâce à cette page "tarifs", que vous n'en trouverez pas et qu'il ne s'agit pas non plus d'un prix libre.</div>

<br>

Au sein du cadeau que vous êtes prêt.e.s à faire ou à vous faire, il y a deux offrants :

- moi vous offrant une pratique de bien être <br>
- vous m'offrant une contrepartie<br>

La contrepartie est ouverte sur divers possibles*, elle indique un souhait de prendre le temps de ressentir au préalable.
Car il arrive que nous n'estimions pas les choses à une valeur égale, ainsi je vous propose dans ce préalable au « service proposé » de trouver un accord commun autour de cette valeur.<br>
<br> 
<div style="border:3px solid #10cbdf;background-color:#e7f9fb;color:#000000;text-align:center; font-weight:bold;">Je suis heureuse de vous donner les valeurs que j'estime à mes prestations : <br>
Stage & Programme VOI : 400€ <br>
Chant de l'Être / Bercement - 1h30 de prestation : 60€<br>
Tente Rouge / Cercle de Femme - 3h ensemble : 10€/personnes <br>
Cérémonie de passage à la carte et autres prestations : 60€/h (prix dégressif selon le nombre d'heure choisie)</div>
<br>

**La (définition) tarifaire s'organise autour de trois aspects :**

- Le service proposé en lui même (chant de l'être, stage découverte de sa voix, bercement, cérémonie de passage, tente rouge, autres, ...)
- Mes frais de déplacement
- Les frais annexes (comme la cuisinière et le gîte pour un stage de 4 jours, location de la piscine pour le bercement dans l'eau, ...)

<br>

<div style="border:3px solid #10cbdf;background-color:#e7f9fb;color:#000000;text-align:center; font-weight:bold;">Bienvenu.e.s à vous dans cette expérience de concertation et d'harmonisation vers la magie d'un moment partagé.</div>
<br>
<div style="font-weight:bold;">* Voici une liste non-exhaustive des contreparties possibles à ce jour :</div>

- L'argent au sens premier représente une monnaie d'échange. C'est avec l'argent (€) que je souhaite prioritairement échanger. 
Toutefois, je ne souhaite pas que l'argent soit un frein à votre désir ; et même s'il répond à mes besoins primaires actuels, je sais aussi qu'il génére diverses dépendances.
- J'accepte la monnaie libre appelée communément la G1, reposant sur le principe d'un revenu universel pour tou.te.s.
- Viennent alors les trocs de type massage, préparation culinaire, offre ou prêt d'objets, entraides, ...  [Retrouvez toutes la liste des contreparties en cliquant ici](https://v-o-i.fr/fr/contreparties.html)

<br>

Je propose aussi lors de stage, programme virtuel, ... d'autres manières d'envisager l'échange et la contre-partie. J'ai à coeur d'expérimenter avec vous dans nos justesses, nos moyens minimums d'accéder à nos désirs, et nos limites. 
