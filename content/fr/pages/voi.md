 <div style="border:3px solid #10cbdf;background-color:#e7f9fb;color:#000000;text-align:center; font-weight:bold;">
    <figure>
    <figcaption>Offrande sonore (extrait)<br>Bonne écoute en pleine conscience</figcaption>
    <audio controls>
        <source src="img/là_1_.mp3" type="audio/mpeg">
        Votre navigateur ne supporte pas la balise audio.
    </audio>
</figure></div>
<br>
<br>
<br>
<br>

<div class="flexArea reverse">


<a class="resumeLink" href="chant.html" style="background-image: linear-gradient(to right, rgba(100%,100%,100%,.8) 0%, rgba(100%,100%,100%,.8) 100%),url('img/bercement-min.JPG');">
    <h2 class="center">Bain sonore</h2>
</a>
<a class="resumeLink" href="stage.html" style="background-image: linear-gradient(to right, rgba(100%,100%,100%,.8) 0%, rgba(100%,100%,100%,.8) 100%),url('img/Cérémonie_de_passage.JPG');">
    <h2 class="center">Accompagnement Voi</h2>
</a>
<a class="resumeLink" href="autres.html" style="background-image: linear-gradient(to right, rgba(100%,100%,100%,.8) 0%, rgba(100%,100%,100%,.8) 100%),url('img/permaculture.JPG');">
    <h2 class="center">Formations</h2></a></div></section><section>