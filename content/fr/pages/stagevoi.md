# Stage 4 jours à la découverte de sa voix
<div style="border:3px solid #10CBDF;background-color:#e7f9fb;color:#000000;text-align:center; font-weight:bold;">
Aimez-vous votre voix, la connaissez-vous vraiment ?<br> Osez-vous parler, chanter en public?</div>
<hr>

Charlène Bazoge vous propose un stage de 4 jours à la découverte de votre voix, votre voix primaire, votre berceau sonore. <br>
<div style=" font-weight:bold;">🕒 Du lundi 27 juin au jeudi 30 juin 2022<br>
📍 à l'écolieu La vilaine, 53143 Maisoncelle du Maine<br>
 <br>
Qui est Charlène Bazoge ?</div>
Originaire des Pays de la Loire, j'ai voyagé et vécu plusieurs années aux quatre coins du monde, pour revenir finalement à mes racines, la Sarthe. Petite, j'étais la chanteuse de la maison. En grandissant, j'ai rejoins la maîtrise de la cathédrale du Mans et la chorale de mon village. Au moment où une once de carrière de chanteuse se profilait, j'ai tout bonnement perdu l'audition. Aujourd'hui, je suis sur le chemin de la responsabilisation de mes affects, de la pleine présence à ce qui est, tout en m'offrant les moyens de vivre des instants non pas satisfaisant mais PLEINEMENT satisfaisant.<br>
Mon rêve est que chaque personne s'offre ce qui est pleinement bon pour lui-elle en chaque instant. Je commence donc par moi en incarnant ce qui me rend la plus joyeuse et vivante : le chant & l'accompagnement par la voix-voie.<br>
<br>
Depuis un an, j'accompagne chaque personne ayant un élan à se découvrir profondément au travers de l'ensemble de ces voi ( voie - voix - voir ) et bien plus.<br>
C'est à dire, sentir sa propre voix s'éveiller en soi et ce, dans toute son authenticité.<br>
Le contrôle mentale, la performance, la recherche de la beauté d'un son ne sont pas les priorités de ces moments uniques. La priorité est la découverte de sa voix puis son expression naturelle.<br>
Ce sont 4 jours dédiés à votre voix - voie afin de plonger à l'intérieur de soi.
<br>
<br>
<div style=" font-weight:bold;">Toutes les raisons d'intégrer un stage de 4 jours :</div>
- S'adonner à la découverte de sa voix
<br>- Aller à son rythme
<br>- Favoriser son autonomie
<br>- Créer toutes les conditions pour se découvrir profondément
<br>- Être pleinement accompagné.e durant ce processus
<br>- Jouer avec des outils pour continuer l'expérience même après ce stage
<br>- Se choisir et vivre une immersion dans un lieu naturel et inédit
<br>
<br>
<div style=" font-weight:bold;">Le programme :</div>
Il n'y a pas de programme ! Mais il y a une trame, celle d'explorer le silence intérieur, apprendre à écouter le silence, éveiller son corps et sa conscience, lâcher prise par l'art intuitif, apprendre à [se] faire confiance, se relier à soi et aux autres, se [dé]livrer avec autonomie ou de manière accompagnée, ralentir, se dépasser, embrasser les peurs, danser avec son timidité, apprendre à poser sa voix parce qu'elle compte tout simplement.<br>
Pendant ces 4 jours, je vous invite à aller vers ce qui est le plus satisfaisant pour répondre à l'intention première de participer à ce stage. Toute une palette de ressources vous sera accessible afin de vous expérimenter pleinement en groupe ou en solo.
<br>
<br>
Chaque stage est unique, car il est le fruit des singularités qui constituent chaque groupe (maximum 5 personnes). Il vous permet d'identifier vos « voi » et d'acquérir les clés pour les révéler au monde.
<br>
<br> 
<div style=" font-weight:bold;">Public apprécié :</div>
- personnes n'aimant pas sa voix
<br>- personnes émettant un jugement sur sa voix (trop aigu, trop grave, trop forte, trop petite,...)
<br>- personnes se disant avoir un lien difficile / compliqué / problématique avec sa voix
<br>- personnes ayant des difficultés à écouter le silence
<br>- personne en recherche de sa voix primaire
<br>
<br>
<div style=" font-weight:bold;">Les informations complémentaires :</div>
- Ces stages sont limités à 5 personnes
<br>- Un hébergement est possible (sur demande à Roselyne au 0676627674)
<br>- Une traiteure peut créer vos repas (sur demande à Roselyne au 0676627674)
<br>
<br>
<div style=" font-weight:bold;">Les prérequis pour intégrer un stage :</div>
- être motivé.e à se découvrir
<br>- sentir un réel désir à s'expérimenter
<br>- <a href="questionnaire.html">remplir le questionnaire (en cliquant ici)</a> 
<br>- <a href="tarifs.html">Lire la page tarif (en cliquant ici)</a> 
<br>
<br>
<div style=" font-weight:bold;">Besoin de plus d'informations ?</div>
Concernant le lieu : Roselyne 0676627674
<br>Concernant le stage : Cha au 0626448171
<br>Concernant autre chose : faites un plouf plouf entre les deux numéros précédents :D
<br>
<br>
En vous souhaitant une journée remplie de ce qui vous comble tout simplement.
