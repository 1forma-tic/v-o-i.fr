# Programme 20 jours à la découverte de sa voix (visio & audio)
<br>
<div style="font-weight:bold;">Du jeudi 1 décembre au mardi 20 décembre</div>
<br>

<div style="font-weight:bold;">Qu'est-ce que le programme 20 jours "A la découverte de sa voix" ?</div>
C'est un programme tout doux qui vous invite chaque jour à découvrir votre voix par le jeu et par une belle dose d'amour de soi. Il commence le 1er du mois par une visio de groupe et se termine 20 jours plus tard avec une visio de clôture. Cette formule est adaptée aux personnes qui ont entre 5mn et 55mn par jour à consacrer à ce programme. Vous décidez en toute autonomie de l'heure à laquelle vous souhaitez écouter les audios qui vous seront transmis sur l'application Telegram.<br>
<br>
<div style="font-weight:bold;">Intention du programme :</div>
- Découvrir sa voix <br>
- Etre pleinement soi-même <br>
- Aller à son rythme <br>
<br>
<div style="font-weight:bold;">Ce que permet ce programme :</div>
- Développer une confiance en soi <br>
- Acquérir une sécurité intérieure<br>
- Respecter son rythme <br>
- Voyager du focus au plan d’ensemble<br>
- Se /célébrer <br>
- S' / écouter<br>
- Se / faire confiance <br>
- Se délivrer des pensées limitantes, auto-sabotages<br>
- Accueillir pleinement ses émotions<br>
- Apprendre en jouant<br>
- Déployer sa créativité<br>
- Favoriser son autonomie <br>
- Être pleinement accompagné.e durant ce processus <br>
<br>
Chaque veille au soir, un audio et un écrit sont envoyés sur l'application Télégram. <img src="img/le_silence_interieur.png" class="portrait"/>
Il est à lire le lendemain quand vous le désirez, en concordance avec votre précieux rythme. Une seule proposition vous est faites par jour afin de rejoindre votre voix plus intimement. Cette invitation peut avoir diverses formes : exercice, expérimentation, jeu, quizz, méditation, visualisation, rituel, challenge, ...<br>
Pendant cette semaine, je réserve des créneaux pour des accompagnements personnalisés et individuels, en lien avec le programme ou la proposition du jour, sous demande de votre part.
<br>
<br>
<div style="font-weight:bold;">Le programme :</div>
Il est construit sur plusieurs piliers qui me sont chers : le silence, les 5 sens, l'intuition et l'ensauvagement.<br>
Le jeudi 1 décembre de 18h à 20h : un cercle d’ouverture en visio est organisé <br>
Ce cercle permet de se centrer pour ces 20 jours transformant. C'est le moment idéal, pour nous présenter d'humain.e à humain.e, d'énoncer au monde nos intentions, de poser nos questions et de créer le cadre d'expérimentation de manière inclusive.<br>
<br>
Au coeur des audios : <br>
Les 2 premiers jours sont consacrés au silence.<br>
Les 15 jours suivants sont une réelle plongée au coeur des 5 sens. Incluant la respiration, les émotions, le pardon, l'amour de soi, l'exploration de son anatomie subtile et l'inclusion du grand écosystème. <br>
Le dernier jour est un espace de célébration privilégié.<br> 
<br>
Le mardi 20 décembre de 18 à 20h : c'est la ronde finale (pépites&co) <br>
Ce temps a pour but de se relier, faire face à nos petits pas conscientisés, découvertes, inspirations et révélations acquises avec autonomie. C'est aussi un moment spécifique où chaque personne rend grâce au précieux qui l'a accompagné lors de ces 20 jours particulièrement intenses.<br>
<br>
<div style="font-weight:bold;">Pré-requis pour intégrer le programme :</div>
- Avoir accès à internet et au numérique (pour les visios et la réception des audios)<br>
- Télécharger sur pc ou/et téléphone l'application Telegram (application gratuite, sécurisée et éthique) <br>
- Être présent.e.s en visio le jeudi 1 décembre 18h-20h et le mardi 20 décembre 18h-20h<br>
- <a href="questionnaire.html">remplir le questionnaire en cliquant ici </a><br>
- Accès réservé aux 6 premières personnes inscrites<br>
- Pré-inscription auprès de Charlène au 0626448171 ou voi@riseup.net<br>
- Etre motivé.e à se découvrir<br>
- Sentir un réel désir à s'expérimenter<br>
- Il n'y a pas besoin de savoir chanter<br>
<br>
<div style="font-weight:bold;">Tarification :</div>
Je vous propose de vivre une expérience avec moi. Pour ce programme je vous présente non pas un tarif, mais une fourchette de prix que je suis ouverte à recevoir pour cet événement.<br>
Voici la fourchette de prix : entre minimum 60€ et maximum 600€<br>
60€ qui après déduction des charges, taxes ne représentent plus que 20€ réellement pour moi. Ces 60€ sont l'expression d'une heure de travail.<br>
600€ qui après déduction des charges, taxes ne représentent plus que 290€ réellement pour moi. Ces 600€ expriment 10h de travail pour concocter ces temps uniques. <br> Et puis dans cette fourchette, il y a vous. Vous qui allez vous ressentir dans votre prix juste. Ce prix qui est en accord avec votre train de vie actuel, ce prix que vous avez réellement envie d'investir pour vous et votre propre développement. <br> 
Dans ma philosophie, le prix n'est pas une limitation en soi. A vous d'être créati.f.ve pour intégrer ce programme.
<br>
<br>

<div style="font-weight:bold;">Conditions de paiement</div>
- Envoyer un accompte de la moitié de la somme que voux souhaitez donner pour ce programme avant le 25 novembre, par virement bancaire, chèque ou espèce.<br>
- Payer la somme restante avant le 22 décembre, par virement bancaire, chèque ou espèce.<br>
<br>
<div style="font-weight:bold;">Les cadeaux : </div>
- Un file Telegram ouvert à l'ensemble des participant.e.s pour échanger sur ce que vous vivez en temps réel.<br>
- Une phrase du jour offerte chaque jour <br>
- Un chant intuitif offert (en audio)<br>
