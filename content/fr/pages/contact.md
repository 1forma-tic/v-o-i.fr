# Mes coordonnées

<img src="img/cha_voi.png" class="portrait"/>


Charlène MACHPY

06 26 44 81 71

<a href="mailto:voi@riseup.net?subject=[contact]&body=Bonjour Cha, je te contacte pour .... . Merci de me recontacter. Cordialement, Une future belle rencontre.">voi@riseup.net</a>

Instagram : [@voi_by_cha](https://www.instagram.com/voi_by_cha/)
<br>
Facebook : [Voi By Cha](https://www.facebook.com/voibycha/)
<br>



# Mes partenaires

## Lieux 

Bonnetable 

Crocus, Jupilles : [le site internet](https://crocus-permaculture.wixsite.com/crocus-permaculture/)

La Vilaine, Maisoncelle du Maine 


## Personnes 

Freda

Millicent [le site internet](https://1forma-tic.fr)

Noémie 

Roselyne

Manon 


# ♡
 **Un MERCI spécial à ces personnes et ces lieux qui m'accueillent dans tout ce que je suis.**


