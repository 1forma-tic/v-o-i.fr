# Formations
<img src="img/cooperation-min.JPG" class="portrait"/> <div style="font-weight : bold;">Formation consentir au quotidien :</div>
A travers les mots et le corps, je vous invite à découvrir l'étendu de votre consentement envers vous même et avec autrui. Oser se dire oui et apprendre à dire non, évoluer les zones de biais de consentement, appréhender les endroits non consentis. A l'aide d'outils ingénieux, je vous invite à éveiller votre conscience au consentement.<br>
<div style="font-weight : bold;">Tarifs : </div>
- sur devis selon le nombre de personnes et la durée souhaitée
<br>
<br><div style="font-weight : bold;">Formation : animation aux jeux bris de glace, energisant, coopération, gestion de conflits</div> Avec neuf années d'expériences dans le domaine, je transmets l'ensemble de mes connaissances et jeux en vous proposant de les vivre en groupe, grandeur nature. <br> Prévoir 2 jours de formation.<br><img src="img/permaculture.JPG" class="portrait"/>
<div style="font-weight : bold;">Tarif : </div> sur devis selon le nombre de personnes et la durée souhaitée
<br>
<br>
<div style="font-weight : bold;">Formation initiation à la permaculture</div>
Durant deux journées riches, je présente l'éthique de la permaculture (l'histoire, les 3 pilliers, les 12 principes, la fleur de la permaculture) ainsi qu'une des méthodes de design. Le but est de vivre chaque enseignement par le corps, le jeu et/ou l'expérimentation. 
<div style="font-weight : bold;">Tarif : </div>sur devis selon le nombre de personnes et la durée souhaitée
<br> 
<br>

<hr> <div style="border:3px solid #10cbdf;background-color:#e7f9fb;color:#000000;text-align:center; font-weight:bold;">
Je suis intéressé.e par l'une des activités présentées,
<br>
je contacte Charlène au 06 26 44 81 71 ou par mail avec <a href="mailto:voi@riseup.net?subject=[contact]&body=Bonjour, je suis intéressé.e par l'une de vos activités. Merci de me recontacter. Cordialement, ta future intéraction"> voi@riseup.net</a></div>
