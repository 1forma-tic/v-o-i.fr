# Accompagnement VOI
<br>
<hr> <div style="border:3px solid #10cbdf;background-color:#e7f9fb;color:#000000;text-align:center; font-weight:bold;">
Connaissez-vous votre voix ?
<br>
Aimez-vous votre voix ?
<br>
Osez-vous parler, chanter ?
</div>
<br> 
<br>
J'accompagne chaque personne ayant un élan à se découvrir profondément au travers de l'ensemble de ces voi ( voie - voix - voir )

<br>
C’est à dire, sentir sa propre voix s’éveiller en soi et ce, dans toute son authenticité. 
<br>Le contrôle mentale, la performance, la recherche de la beauté d'un son ne sont pas les priorités de ces moments uniques. La priorité est la découverte de sa voix puis son expression naturelle.
<br>
<br>
<div style="font-weight:bold;">Il existe 3 formules différentes : </div><img src="img/stage_chant_de_l_être.jpg" class="portrait"/>
<div style="font-weight:bold;">- Le stage 4 jours :</div> C'est un stage immersif et radical. Il est en présentiel dans des lieux naturels (en pleine forêt, dans des éco-lieux, à la mer, au bord de rivières,...). Ce stage est accessible à 6 personnes maximum. Et peut être personnalisé pour une personne seule. <br>
<div style="font-weight:bold;">- Le programme 8 jours :</div> C'est un programme intensif en visio et en audio. L'expérience commence le dimanche soir en visio. Puis chaque matin, un audio avec plusieurs exercices-invitations-expérimentations, est envoyé afin de vivre en toute autonomie chaque processus. Le dimanche soir suivant, une clôture du programme se fait en visio. L'expérience se vit de 1 à 12 personnes maximum. C'est un programme idéale et adapté aux personnes ayant plusieurs heures par jour à se vouer à cette activité.<br>
<div style="font-weight:bold;">- Le programme 20 jours : </div>C'est un programme tout doux. Il commence le 1er du mois et se termine 20 jours après. Une visio de début et de fin sont organisées. Une seule proposition est envoyée par jour. L'expérience se vit de 1 à 12 personnes maximum. Cette formule est adaptée aux personnes qui ont entre 5mn et 55mn (max) par jour à consacrer à ce programme.
<br>
<br>
<hr> <div style="border:3px solid #10cbdf;background-color:#e7f9fb;color:#000000;text-align:center; font-weight:bold;"> Quelle que soit la formule choisie, l'idée est de plonger à l'intérieur de soi et de découvrir sa voix à son rythme.</div>
<br>
<div style="font-weight:bold;">Toutes les raisons d'intégrer un stage ou un programme : </div>
- S'adonner à la découverte de sa voix <br>
- Aller à son rythme <br>
- Favoriser son autonomie <br>
- Créer toutes les conditions pour se découvrir profondément <br>
- Etre pleinement accompagné.e durant ce processus<br>
- Jouer avec des outils pour continuer l'expérience même après ce stage<br><img src="img/aimer_sa_voix.png" class="portrait"/>
- Se choisir et vivre son intention de manière inédite<br>
<br>

<div style="font-weight:bold;">Le programme : </div>
Il n'y a pas de programme précis ! Cependant, l'accompagnement est global. Il y a une trame, celle d'explorer le silence intérieur, apprendre à écouter le silence, éveiller son corps et sa conscience, lâcher prise par l'art intuitif, apprendre à se faire confiance et faire confiance, se relier à soi et aux autres, se livrer et se délivrer en autonomie ou de manière accompagnée.
<br> 
<br>
<div style="font-weight:bold;">Les intentions :</div>
- découvrir sa voix <br>
- vivre le moment présent<br>
- cheminer à son rythme vers soi<br>
<br>
<div style="font-weight:bold;">Les petits + de la formule stage (accessible sur demande 1 semaine avant le stage): </div>
- Un hébergement est possible<br>
- Une traiteure peut créer vos repas (selon votre régime et vos besoins)<br>
- Un service garderie est envisageable<br>
- des services complémentaires sont envisageables avec des professionnelles : massage, cérémonie de passage, bercement, ...<br>
<br>

<div style="font-weight:bold;"> Les prérequis pour intégrer un stage </div>
- <a href="questionnaire.html">remplir le questionnaire ici </a><br>
- être motivé.e à se découvrir <br>
- sentir un réel désir à s'expérimenter <br>
- il n'y a pas besoin de savoir chanter <br>
<br>
<hr>

# Espace témoignages

"J'ai beaucoup apprécié l'accompagnement de Charlène pendant les 8 jours qu'à duré le module. A travers les vidéoconférences de début et de fin, ainsi que l'ensemble des exercices quotidiens qu'elle m'a proposé chaque matin, j'ai comme resstenti sa présence constante à mes côtés pour me suivre dans ces pratiques qui traversent l'ensemble des activités quotidiennes... à la fois dévouée et exigeante, inspirée et espiègle. J'ai particulièrement apprécié la flexibilité et la cohérence subtile du parcours proposé ainsi que la grande diversité des outils mis à disposition; j'ai aussi aimé la finesse et la justesse de son regard ainsi que la qualité de ses chants intuitifs qui m'ont permis de vivre pendant cette semaine un voyage de guérison profonde et de reconnexion avec la beauté intime de "mon" être. Un grand merci à toi, Charlène." <br>L - Programme 8 jours d'octobre 2022 - visio<br>
<br>
"Cheminer à mon rythme était un délice. J'ai appris à poser ma voix, le soir même, ma famille m'entendait, m'écoutait. Ton programme est fabuleux, intelligemment construit. Une vrai pépite! Après 2 ans, les traces de ton stage sont encore présentes. Merci de m'avoir autant inspiré, soutenu. M - Stage 4 jours 2021 - présentiel
<br>
<br>

# Tarifs

- Stage 4 jours en présentiel : <div style="font-weight:bold;">600€</div> (sans hébergement)
- Programme VOI 8 jours : <div style="font-weight:bold;">450€</div>
- Programme VOI 20 jours : <div style="font-weight:bold;">450€</div>
<br>
<br>
<div style="border:3px solid #10cbdf;background-color:#e7f9fb;color:#000000;text-align:center; font-weight:bold;">
Je saute le pas maintenant !<br>
Je fais la demande d'un accompagnement <br>
Ici 06 26 44 81 71 ou voi@riseup.net</a></div>
</div>
<br>
<hr>

# Les prochains stages & programmes
