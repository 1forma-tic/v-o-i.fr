# 2024 ensemble
<hr>
<br>

<div style="border:3px solid #10CBDF;background-color:#e7f9fb;color:#000000;text-align:center; font-weight:bold;">
🔍 Bain sonore<br>
🕒 Vendredi 17 mai 18h-19h30<br>
📍 Bonnétable</div>
<hr>
<br>

# Rendez-vous passés

<div style="border:3px solid #065159;background-color:#e6edee;color:#000000;text-align:center; font-weight:bold;">
🔍 Programme 8 jours - A la découverte de sa VOI <br>
🕒 Du Dimanche 16 avril 2023 au Dimanche 23 janvier 2023<br>
📍 Visio & audio <br>
<a href="huitjoursavril.html">Toutes les informations en cliquant ici</a></div>
<hr>
<br>

<div style="border:3px solid #065159;background-color:#e6edee;color:#000000;text-align:center; font-weight:bold;">
🔍 Programme 20 jours - A la découverte de sa VOI <br>
🕒 Du Mardi 1 février au Lundi 20 février 2023<br>
📍 Visio & audio <br>
<a href="vingtjoursfevrier.html">Toutes les informations en cliquant ici</a></div>
<hr>
<br>

<div style="border:3px solid #10CBDF;background-color:#e7f9fb;color:#000000;text-align:center; font-weight:bold;">
🔍 Bain sonore<br>
🕒 Vendredi 3 février 14-15h30<br>
📍 Bonnétable</div>
<hr>
<br>

<div style="border:3px solid #065159;background-color:#e6edee;color:#000000;text-align:center; font-weight:bold;">
🔍 Programme 8 jours - A la découverte de sa VOI <br>
🕒 Du Dimanche 8 janvier 2023 au Dimanche 15 janvier 2023<br>
📍 Visio & audio <br>
<a href="huitjoursjanvier.html">Toutes les informations en cliquant ici</a></div>
<hr>
<br>

<div style="border:3px solid #10CBDF;background-color:#e7f9fb;color:#000000;text-align:center; font-weight:bold;">
🔍 Bain sonore Aquatique<br>
🕒 Mardi 29 novembre 18-20h<br>
📍 Centre aquatique Plouf 46 rue du 11 Novembre - 72500 MONTVAL-SUR-LOIR <br>
Sur réservation à l'entrée</div>