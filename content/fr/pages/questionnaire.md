# Questionnaire
<br>
<div style="font-weight:bold;">Quelle est mon intention à poser ces questions ?</div>
- être plus au contact de vous, de votre réalité<br>
- établir un premier lien de confiance<br>
<br>
J'appelle donc toute votre authenticité à s'exprimer à travers vos réponses. Je me tiens aussi à votre disposition pour vous aider à clarifier vos réponses (sur demande de votre part).
<br>
Voici les questions auxquelles je vous propose de répondre (au cours d'un mail ou d'un appel ou toutes autres formes qui vous raviront) :<br>

- Quelle est mon rapport avec ma voix - voie ?
- Quelle est mon intention à vivre ce stage/programme ?<br>
- Qu'est-ce que je viens produire par ce stage/programme ?<br>
- En me donnant quels moyens précis ?<br>
- Suis-je consentent.e à me servir, de manière pleinement satisfaisante, des ressources présentes au stage/programme ?<br>
<br>
<div style="font-weight:bold;">Merci de m'envoyer vos réponses par mail à voi@riseup.net, par téléphone au 0626448171 ou en face à face.</div>
<br><br>
À la fin du stage/programme, je vous invite à répondre à un second questionnaire. Le voici :<br>

- Quelle est mon rapport avec ma voix - voie ?
- Ai-je répondu à mes intentions ?<br>
- Qu'ai-je produit par ce stage/programme ?  <br>
- Quels moyens me suis-je donné.e.s pour atteindre ma pleine satisfaction ?<br>
- Me suis-je pleinement servi des ressources ? 
