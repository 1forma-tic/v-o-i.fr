# A propos
Je m'appelle Charlène Machpy.


<img src="img/cha.jpg" class="portrait"/>Je suis chanteuse depuis toujours, doula depuis 2016 => https://charlenedoula.fr/ , créatrice de VOI depuis 2021 et autrice d'une conférence gesticulée depuis 2024.
Habitante d'un collectif de vie à Bonnetable (nord Sarthe), je suis maman depuis 2023. Je vis une famille atypique pour mon plus grand bonheur. 

Enfant je chantais, adulte je chante toujours ! Formée à la maitrise de la cathédrale du Mans puis auprès de différentes cheffes de coeur en France et à l'étranger, j'ai rejoint plusieurs groupes de divers registres musicaux (reggea, opéra, variété française, italienne, chants traditionnels hongrois, bulgares, chorales féministes, chorales militantes). Aujourd'hui, je me concentre sur les bains sonores et les accompagnement VOI

- Mes qualités (selon moi) : joyeuse, authentique, force d'adaptation, à l'écoute, empathique

- Mes qualités (selon mes proches) : positive, créative, courageuse, drôle, persévérante

- Mes axes d'amélioration (selon moi) : impatiente, utopiste, gourmande

- Mes axes d'amélioration (selon mes proches) : exigeante, naïve


# Mes intentions


J'ai le désir que chaque personne s'offre ce qui est pleinement bon pour lui-elle-iel-ielle en chaque instant. Je commence donc par moi en incarnant ce qui me rend la plus joyeuse et vivante : être doula, chanter et offrir mes services d'accompagnement VOI. 


J'ai le profond désir d'accompagner avec tout ce que je suis, chaque personne qui ressent un "GRAND OUI !!"  à la lecture d'une ou plusieurs de mes propositions faites au travers de ce site internet qui présente qu'une partie de l'iceberg de mes ressources et compétences.
