# Bain sonore
<br>
<img src="img/chant_de_l_etre.jpg" class="portrait"/>

<div style="font-weight:bold;">Qu'est-ce qu'un bain sonore ?</div>
Je mêle un chant personnalisé, aussi appelé : chant intuitif, chant spontané, chant aléatoire, chant de l'être; avec des sons naturels que j'orchestre au fur et à mesure de ce temps de relaxation auditive.<br>Le jeu avec les élèments naturels (sable, eau, terre, mousse, coquillages et bien d'autres) permet de se relier de manière intemporelle à soi et au vivant.  <br> Ce temps suspendu fait appel au moment présent et au lâcher prise le plus total. <br>C'est une bulle sonore unique et atypique à [s']offrir sans modération.<br>
<br>
<div style="font-weight:bold;">Les lieux</div>
J'aime chanter dans des lieux calmes, naturels et inédits; de jour comme de nuit. Ce lieu est peut-être le vôtre ou bien un lieu atypique que nous avons en commun... Parlons-en ensemble.
<br>
<br>
<div style="font-weight:bold;">Quelles sont les informations complémentaires ?</div>
Un bain sonore dure en général 1h30.<br>
Prévoir des habits adaptés aux lieux choisis et à la météo du moment <br>
Venir avec tous les outils et objets sécurisant reste une bonne idée (bouteille d'eau, coussin, tapis de yoga,...)
<br>
<br>

# Espace témoignages 
<img src="img/P8020101-min.JPG" class="portrait"/> 
- "Je ne connaissais pas le chant intuitif, qu'est ce que ça fait du bien! C'est super. Merci ! Je vous dis à bientôt car j'ai envie de revivre un moment comme celui-ci avec ma petite fille." Maïté*, Novembre 2022 Château du Loir

- "J'ai vécu le chant en restant assise. Mon corps s'est tellement relaxé que je n'étais plus bien installée assise. Je n'ai pas osé m'allonger, pourtant tout était préparé, quel dommage. C'est bizarre, je me sens là. Présente. Ancrée." Sylvaine*, Novembre 2022 Château du Loir

- "Je suis une personne tendue, je suis une vraie pile, toujours en action. J'ai failli rire au début de la séance mais après votre chant intuitif, j'ai l'impression d'être détendue pour la première fois depuis longtemps. Je ne connaissais pas cette pratique. C'est surprenant... Merci !" Josephine*, Novembre 2022 Château du Loir

- "Cha, tes chants sont une médecine, ma médecine. Merci pour ta présence infaillible et ton ancrage qui m'aide à moi-même m'ancrer dans le moment présent" Louis*, Septembre 2022 Isère

- "Merci pour ta puissance, ta beauté et tes sons fantastiques." Basile*, juillet 2022 au Val de Chalvagne

- "Alors ça c'est du réveil !", Loïc*, juillet 2022 au Val de Chalvagne

- "Présence Accompagnement Retour à l’instant Merci madame Cha 🙏" Albertine, avril 2022 à Jupilles

- "Un vrai voyage sonore", Anne, avril 2022 à Jupilles

. * Prénom d'emprunt pour assurer l'anonymat demandé.
<br>
<br>

# Tarifs 

- Bain sonore personnalisé pour une personne : <div style="font-weight:bold;">90€/1h30</div><br>
- Bain sonore de groupe (à partir de 6 personnes): <div style="font-weight:bold;">15€/personne/1h30</div>
<br>
<br>
<hr>
<div style="border:3px solid #10cbdf;background-color:#e7f9fb;color:#000000;text-align:center; font-weight:bold;">
Je veux mon bain sonore ! <br>Contactes moi au 06 26 44 81 71 ou voi@riseup.net</a></div><hr>
<br>


# Prochains rendez-vous 
<hr>

<div style="border:3px solid #10CBDF;background-color:#e7f9fb;color:#000000;text-align:center; font-weight:bold;">
🔍 Bain sonore de groupe<br>
🕒 Vendredi 17 mai 18h-19h<br>
📍 Bonnétable</div>