# Programme 8 jours à la découverte de sa voix (visio & audio)
<br>
<div style="font-weight:bold;">Du dimanche 20 novembre au dimanche 27 novembre</div>
<br>
<div style="font-weight:bold;">Qu'est-ce que le programme 8 jours "A la découverte de sa voix" ?</div>
C'est un programme intensif en visio et en audio. L'expérience commence le dimanche soir en visio. Puis chaque veille au soir, un audio avec plusieurs exercices-invitations-expérimentations, vous sont envoyés afin de vivre en toute autonomie votre processus le lendemain au moment opportun pour vous. Le dimanche soir suivant, une clôture du programme se fait en visio. L'expérience se réalise seul.e mais au-sein d'un groupe variant de 2 à 6 personnes maximum. C'est un programme idéale et adapté aux personnes ayant plusieurs heures par jour pour soi et plus précisément pour la découverte de sa voix.<br>
<br>
<div style="font-weight:bold;">Intention du programme :</div>
- Découvrir sa voix <br>
- Etre pleinement soi-même <br>
<br>
<div style="font-weight:bold;">Ce que permet ce programme innovant :</div>
- Développer une confiance en soi <br>
- Acquérir une sécurité intérieure<br>
- Respecter son rythme <br>
- Voyager du focus au plan d’ensemble<br>
- Se /célébrer <br>
- S' / écouter<br>
- Se / faire confiance <br>
- Se délivrer des pensées limitantes, auto-sabotages<br>
- Accueillir pleinement ses émotions<br>
- Apprendre en jouant<br>
- Déployer sa créativité<br>
- Favoriser son autonomie <br>
- Être pleinement accompagné.e durant ce processus <br>
<br>
Chaque jour, plusieurs propositions prennent différentes formes : exercices, expérimentations, jeux, quizz, méditations, visualisations, ...<br>
Pendant cette semaine, je réserve des créneaux pour des accompagnements personnalisés et individuels, en lien avec le programme ou la proposition du jour, sous demande de votre part.
<br>
<br>
<div style="font-weight:bold;">Le programme :</div><br>
Le dimanche 20 novembre 18h-20h : cercle d’ouverture en visio <br>
Ce cercle permet de se centrer pour cette semaine exceptionnelle. C'est le moment idéal, pour poser nos intentions et créer le cadre d'expérimentation adapté à chacun.e.<br>
<br>
<img src="img/Prendre_soin_de_sa_voix.png" class="portrait"/>Du lundi 21 novembre au samedi 26 novembre : proposition audio<br>
Je vous envoie un audio la veille pour l'écouter au petit matin. Afin de laisser la surprise jusqu'au bout je vous présente uniquement le titre des journées :<br>
Le lundi 21 novembre :  silençons nous<br>
Le mardi 22 novembre : honorer son berceau sonore<br>
Le mercredi 23 novembre : respirer ensemble<br>
Le jeudi 24 novembre : toucher sa voix<br>
Le vendredi 25 novembre : entendons nous<br>
Le samedi 26 novembre : imager sa voix<br>
<br>
Le dimanche 27 novembre 18-20h : ronde finale (pépites&co) <br>
Ce temps a pour but de se relier, faire face à nos petits pas conscientisés, découvertes, inspirations et révélations acquises avec autonomie. C'est aussi un moment spécifique où chaque personne rend grâce au précieux qui l'a accompagné lors de cette semaine particulièrement intense.<br>
<br>
Pendant toute cette semaine, un file Telegram est ouvert à l'ensemble des participant.e.s pour vous permettre d'échanger sur ce que vous vivez. C'est aussi sur cette application où les audios seront déposés. (Adaptable par mail sous demande) <br>
<br>
<div style="font-weight:bold;">Pré-requis pour intégrer le programme :</div>
- Avoir accès à internet et au numérique (pour les visios et la réception des audios)<br>
- Télécharger sur pc ou/et téléphone l'application Telegram (application gratuite, sécurisée et éthique) <br>
- Être présent.e en visio le dimanche 20 novembre de 18h à 20h et le dimanche 27 novembre de 18h à 20h<br>
- <a href="questionnaire.html">remplir le questionnaire en cliquant ici </a><br>
- Accès réservé aux 6 premières personnes inscrites<br>
- Pré-inscription auprès de Charlène au 0626448171 ou voi@riseup.net<br>
- Etre motivé.e à se découvrir<br>
- Sentir un réel désir à s'expérimenter<br>
- Il n'y a pas besoin de savoir chanter<br>
<br>
<div style="font-weight:bold;">Tarification :</div>
Je vous propose de vivre une expérience avec moi. Pour ce programme je vous présente non pas un tarif, mais une fourchette de prix que je suis ouverte à recevoir pour cet événement.<br>
Voici la fourchette de prix : entre minimum 60€ et maximum 600€<br>
60€ qui après déduction des charges, taxes ne représentent plus que 20€ réellement pour moi. Ces 60€ sont l'expression d'une heure de travail.<br>
600€ qui après déduction des charges, taxes ne représentent plus que 290€ réellement pour moi. Ces 600€ expriment 10h de travail pour concocter ces temps uniques. <br> Et puis dans cette fourchette, il y a vous. Vous qui allez vous ressentir dans votre prix juste. Ce prix qui est en accord avec votre train de vie actuel, ce prix que vous avez réellement envie d'investir pour vous et votre propre développement. <br> 
Dans ma philosophie, le prix n'est pas une limitation en soi. A vous d'être créati.f.ve pour intégrer ce programme.
<br>
<br>
<div style="font-weight:bold;">Conditions de paiement</div>
- Envoyer un accompte de la moitié de la somme que je souhaite donner pour ce programme avant le 15 novembre, par virement bancaire, chèque ou espèce.<br>
- Payer la somme restante avant le 30 novembre, par virement bancaire, chèque ou espèce. <br>
<br>
<div style="font-weight:bold;">Les cadeaux : </div>
- Un file Telegram ouvert à l'ensemble des participant.e.s pour échanger sur ce que vous vivez en temps réel.<br>
- Une phrase du jour offerte chaque jour <br>
- Un chant intuitif offert (en audio)
